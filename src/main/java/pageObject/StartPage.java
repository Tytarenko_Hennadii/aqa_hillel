package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StartPage extends BasePage {

    public StartPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='global-header-section mod-right']/a[contains(text(),'Войти')]")
    private WebElement startLoginButton;

    public WebElement getStartLoginButton() {
        return startLoginButton;
    }
}