package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AllDesks extends BasePage {

    public AllDesks(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//span[contains(text(),'Создать новую доску…')]")
    private WebElement createDesk;

    public WebElement getCreateDesk() {
        return createDesk;
    }

    @FindBy(xpath = "//span[@class='member']")
    private WebElement userIcon;

    public WebElement getUserIcon() {
        return userIcon;
    }

    @FindBy(xpath = "//span[@class='pop-over-header-title']")
    private WebElement userName;

    public WebElement getUserName() {
        return userName;
    }

    @FindBy(xpath = "//input[@id='boardNewTitle']")
    private WebElement deskName;

    public WebElement getDeskName() {
        return deskName;
    }

    @FindBy(xpath = "//input[@class='primary wide js-submit']")
    private WebElement submit;

    public WebElement getSubmit() {
        return  submit;
    }

    @FindBy(xpath = "//span[@class='board-tile-details is-badged']//span[contains(text(),'Lists')]")
    private WebElement createdDeskName;

    public WebElement getCreatedDeskName() {
        return  createdDeskName;
    }

}

