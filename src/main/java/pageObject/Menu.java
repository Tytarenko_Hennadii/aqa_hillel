package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Menu extends BasePage{

    public Menu(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[@class='board-menu-navigation-item-link js-open-more']")
    private WebElement moreOption;

    public WebElement getMoreOption() {
        return moreOption;
    }


    @FindBy(xpath = "//a[contains(text(),'Закрыть доску…')]")
    private WebElement closeDeskLink;

    public WebElement getCloseDeskLink() {
        return closeDeskLink;
    }

    @FindBy(xpath = "//input[@class='js-confirm full negate']")
    private WebElement closeButton;

    public WebElement getCloseButton() {
        return closeButton;
    }

}
