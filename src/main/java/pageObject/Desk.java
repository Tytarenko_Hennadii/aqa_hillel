package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk extends BasePage {

    public Desk(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='board-header u-clearfix js-board-header']//span[@class='board-header-btn-text']")
    private WebElement createdDeskName;

    public WebElement getCreatedDeskName() {
        return createdDeskName;
    }

    @FindBy(xpath = "//input[@class='list-name-input']")
    private WebElement listNameInput;

    public WebElement getListNameInput() {
        return listNameInput;
    }

    @FindBy(xpath = "//input[@value='Сохранить']")
    private WebElement saveListButton;

    public WebElement getSaveListButton() {
        return saveListButton;
    }

    @FindBy(xpath = "//div[@class='list js-list-content']//textarea[1]")
    private WebElement firstListName;

    public WebElement getFirstListName() {
        return firstListName;
    }

    @FindBy(xpath = "(//div[@class='list js-list-content']//textarea)[2]")
    private WebElement secondListName;

    public WebElement getSecondListName() {
        return secondListName;
    }


}
