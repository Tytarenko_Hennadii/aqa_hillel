package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeletePage extends BasePage {

    public DeletePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[@class='quiet js-delete']")
    private WebElement removeLink;

    public WebElement getRemoveLink() {
        return removeLink;
    }

    @FindBy(xpath = "//input[@class='js-confirm full negate']")
    private WebElement removeButton;

    public WebElement getRemoveButton() {
        return removeButton;
    }

    @FindBy(xpath = "//div[@class='big-message quiet']//h1")
    private WebElement removeMessage;

    public WebElement getRemoveMessage() {
        return removeMessage;
    }

}
