package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class Card extends BasePage {

    public Card(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@id='board']/div[1]//a[contains(text(),'Добавить карточку…')]")
    private WebElement addCard;
    public WebElement getAddCard() {
        return addCard;
    }

    @FindBy(xpath = "//div[@class='card-composer']//textarea")
    private WebElement cardName;
    public WebElement getCardName() {
        return cardName;
    }
    @FindBy(xpath = "//div[@class='cc-controls-section']//input")
    private WebElement addButton;
    public WebElement getAddButton() {
        return addButton;
    }
    @FindBy(xpath = "//div[@class='list-card-details']//span[contains(text(),'New Card')]")
    private WebElement card;
    public WebElement getCard() {
        return card;
    }
    @FindBy(xpath = "//div[@class='card-detail-edit edit']//textarea")
    private WebElement cardDescription;
    public WebElement getCardDescription() {
        return cardDescription;
    }
    @FindBy(xpath = "//div[@class='edit-controls u-clearfix']/input[@value='Сохранить']")
    private WebElement saveDescription;
    public WebElement getSaveDescription() {
        return saveDescription;
    }
    @FindBy(xpath = "//a[@class='icon-lg icon-close dialog-close-button js-close-window']")
    private WebElement closeCard;
    public WebElement getCloseCard() {
        return closeCard;
    }
    @FindBy(xpath = "//div[@title='Эта карточка с описанием.']")
    private WebElement descriptionIcon;
    public WebElement getDescriptionIcon() {
        return descriptionIcon;
    }

}
