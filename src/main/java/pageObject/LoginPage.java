package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//input[@id='user']")
    private WebElement email;

    public WebElement getEmail() {
        return email;
    }

    @FindBy(xpath = "//input[@id='password']")
    private WebElement password;

    public WebElement getPassword() {
        return password;
    }

    @FindBy(xpath = "//input[@id='login']")
    private WebElement loginloginbutton;

    public WebElement getLoginloginbutton() {
        return loginloginbutton;
    }

}
