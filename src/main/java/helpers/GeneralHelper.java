package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GeneralHelper {

    public static void click(WebElement element, String message){
        element.click();
        System.out.println("Click on " +message);
    }

    public static void typeToElement(WebElement element, String message){
        element.sendKeys(message);

    }

    public static void waitToElement(By element, WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
}