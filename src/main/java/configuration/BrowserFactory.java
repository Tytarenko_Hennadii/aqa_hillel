package configuration;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.net.URL;

import java.net.MalformedURLException;

import static java.lang.System.setProperty;



public enum BrowserFactory {

        CHROME{
            public final ThreadLocal<EventFiringWebDriver> DRIVER_THREAD_LOCAL = new InheritableThreadLocal<>();

            public ThreadLocal<EventFiringWebDriver> create() throws MalformedURLException {
            //public EventFiringWebDriver create() throws MalformedURLException {
                setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                DesiredCapabilities capability = DesiredCapabilities.chrome();
                DRIVER_THREAD_LOCAL.set(new EventFiringWebDriver(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability)).register(new WebdriverLogger()));
                //EventFiringWebDriver driver = new EventFiringWebDriver(new ChromeDriver());
                //driver.register(new WebdriverLogger());
                return DRIVER_THREAD_LOCAL;
            }
        };
//        IE{
//            public EventFiringWebDriver create(){
////                return new InternetExplorerDriver();
//            }
//        },
//        FIREFOX{
//            public EventFiringWebDriver create(){
////                return new FirefoxDriver();
//            }
//       };

    public ThreadLocal<EventFiringWebDriver> create() throws MalformedURLException {
        return null;
        }
    }
