package first_tests;

import configuration.BrowserFactory;

import configuration.WebdriverLogger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.assertj.core.api.Assertions;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import static helpers.GeneralHelper.*;



import pageObject.*;

public class TrelloTests {
    EventFiringWebDriver driver;
    WebdriverLogger webdriverLogger;
    AllDesks alldesks;
    StartPage startpage;
    LoginPage loginpage;
    Desk desk;
    Menu menu;
    DeletePage deletepage;
    Card card;


    @BeforeMethod
    public void precondition() throws MalformedURLException {

        driver = BrowserFactory.CHROME.create().get();

        webdriverLogger = new WebdriverLogger();
        driver.register(webdriverLogger);

        driver.get("https://trello.com/");
        driver.manage().window().maximize();

        startpage = PageFactory.initElements(driver, StartPage.class);
        loginpage = PageFactory.initElements(driver, LoginPage.class);
        alldesks = PageFactory.initElements(driver, AllDesks.class);
        desk = PageFactory.initElements(driver, Desk.class);
        menu = PageFactory.initElements(driver, Menu.class);
        deletepage = PageFactory.initElements(driver, DeletePage.class);
        card = PageFactory.initElements(driver, Card.class);


       /* alldesks = new AllDesks(driver);
        startpage = new StartPage(driver);
        loginpage = new LoginPage(driver);
        desk = new Desk(driver);
        menu = new Menu(driver);
        deletepage = new DeletePage(driver);
        card = new Card(driver);
*/
    }

    @AfterMethod
    public void postcondition(){
        driver.close();
        driver.quit();
    }

    @Test
    public void login(){
        String Name = "TestName (testname65)";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(startpage.getStartLoginButton(), "login button");
        typeToElement(loginpage.getEmail(), "hazanov@i.ua");
        typeToElement(loginpage.getPassword(), "testname2615386");
        click(loginpage.getLoginloginbutton(), "login button");
        click(alldesks.getUserIcon(), "User icon");
        String LoginName = alldesks.getUserName().getText();
        Assert.assertEquals(Name, LoginName);
    }


    @Test
    public void deskcreation() throws InterruptedException {
        String Name = "TestDesk_1";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(startpage.getStartLoginButton(), "login button");
        typeToElement(loginpage.getEmail(), "hazanov@i.ua");
        typeToElement(loginpage.getPassword(), "testname2615386");
        click(loginpage.getLoginloginbutton(), "login button");
        click(alldesks.getCreateDesk(), "Create New Desk");
        typeToElement(alldesks.getDeskName(), "TestDesk_1");
        click(alldesks.getSubmit(), "Create");
        Thread.sleep(1000);
        String CreatedName = desk.getCreatedDeskName().getText();
        assertThat(Name, equalTo(CreatedName));
    }


    @Test
    public void deskremove() throws InterruptedException {
        click(startpage.getStartLoginButton(), "login button");
        typeToElement(loginpage.getEmail(), "hazanov@i.ua");
        typeToElement(loginpage.getPassword(), "testname2615386");
        click(loginpage.getLoginloginbutton(), "login button");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(alldesks.getCreateDesk(), "Create New Desk");
        typeToElement(alldesks.getDeskName(), "TestDesk_2");
        click(alldesks.getSubmit(), "Create");
        Thread.sleep(1000);
        click(menu.getMoreOption(), "More...");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(menu.getCloseDeskLink(), "Close link");
        click(menu.getCloseButton(), "Close");
        click(deletepage.getRemoveLink(), "Remove link");
        click(deletepage.getRemoveButton(), "Remove button");
        Assertions.assertThat(deletepage.getRemoveMessage().getText()).isEqualToIgnoringCase("TestDesk_2 закрыта.");
    }

    @Test
    public void listscreation() throws InterruptedException {
        String list1 = "List_1";
        String list2 = "List_2";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(startpage.getStartLoginButton(), "login button");
        typeToElement(loginpage.getEmail(), "hazanov@i.ua");
        typeToElement(loginpage.getPassword(), "testname2615386");
        click(loginpage.getLoginloginbutton(), "login button");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(alldesks.getCreateDesk(), "Create New Desk");
        typeToElement(alldesks.getDeskName(), "Lists");
        click(alldesks.getSubmit(), "Create");
        Thread.sleep(1000);
        typeToElement(desk.getListNameInput(), "List_1");
        click(desk.getSaveListButton(), "Create");
        typeToElement(desk.getListNameInput(), "List_2");
        click(desk.getSaveListButton(), "Create");
        String CreatedList1 = desk.getFirstListName().getText();
        String CreatedList2 = desk.getSecondListName().getText();
        assertThat(list1, equalTo(CreatedList1));
        assertThat(list2, equalTo(CreatedList2));
    }


    @Test
    public void cardcreation() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(startpage.getStartLoginButton(), "login button");
        typeToElement(loginpage.getEmail(), "hazanov@i.ua");
        typeToElement(loginpage.getPassword(), "testname2615386");
        click(loginpage.getLoginloginbutton(), "login button");
        click(alldesks.getCreatedDeskName(), "Created Desk");
        click(card.getAddCard(), "Add card link");
        typeToElement(card.getCardName(), "New Card");
        click(card.getAddButton(), "Add card link");
        click(card.getCard(), "Card");
        typeToElement(card.getCardDescription(), "Description");
        click(card.getSaveDescription(), "Save");
        click(card.getCloseCard(), "Close");
        Assert.assertTrue(card.getDescriptionIcon().isDisplayed());

    }
}
